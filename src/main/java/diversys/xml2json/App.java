package diversys.xml2json;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class App {
    public static void main( String[] args ) {
        //String textXml = "<mydocument has=\"an attribute\"><and><many>elements</many><many>more elements</many></and><plus a=\"complex\">element as well</plus></mydocument>";
        String textXml = "<xmlBitStreamInfoResponse><Channels><PVC><MAC Client_MAC=\"c4ea1df0e65e\" Client_IP=\"\" VMAC=\"0202f1830d96\"/><PVC VlanID=\"2966\" vci=\"0\" vpi=\"0\"/></PVC><PVC><MAC Client_MAC=\"c4ea1df0e65e\" Client_IP=\"10.89.223.207\" VMAC=\"0202f1830d96\"/><PVC VlanID=\"2967\" vci=\"0\" vpi=\"0\"/></PVC><PVC><MAC Client_MAC=\"c4ea1df0e65e\" Client_IP=\"10.88.223.199\" VMAC=\"0202f1830d96\"/><PVC VlanID=\"2968\" vci=\"0\" vpi=\"0\"/></PVC></Channels><Option82 Platform=\"HUAWEI M5600\" Slot=\"6\" IP=\"172.26.0.29\" Port=\"54\" SID=\"\" Name=\"VU_GUNJA_HM01\"/><Port SincType=\"\" Status=\"UP\" ProfileName=\"v-P2t6-23/4-d42i105-a\"><PhysMeasurement AttainableBitrateUS=\"6226000\" AttainableBitrateDS=\"36084000\" BitrateDS=\"\" BitrateUS=\"\" SnrDS=\"130\" PowerDS=\"100\" SnrUS=\"110\" PowerUS=\"186\" AttenuationDS=\"186\" AttenuationUS=\"64\"/></Port></xmlBitStreamInfoResponse>";

        DocumentBuilderFactory fctr = DocumentBuilderFactory.newInstance();
        DocumentBuilder bldr = null;
        Document xmlDocument = null;
        try {
            bldr = fctr.newDocumentBuilder();
            InputSource insrc = new InputSource(new StringReader(textXml));
            xmlDocument = bldr.parse(insrc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Xml2JsonConverter xml2JsonConverter = new Xml2JsonConverter();
        System.out.println( xml2JsonConverter.convert(xmlDocument).toString() );
    }
}
