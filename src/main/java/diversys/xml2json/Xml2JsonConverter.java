package diversys.xml2json;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import javax.json.*;
import java.util.HashMap;
import java.util.Map;

public class Xml2JsonConverter {

    private JsonBuilderFactory jsonBuilderFactory;

    public Xml2JsonConverter() {
        this.jsonBuilderFactory = Json.createBuilderFactory(null);
    }

    public Xml2JsonConverter(Map<String, ?> config) {
        this.jsonBuilderFactory = Json.createBuilderFactory(config);
    }

    public JsonObject convert(Node node) {
        JsonObjectBuilder jsonObjectBuilder = this.jsonBuilderFactory.createObjectBuilder();

        // recursively process xml document and build Json object
        if (node instanceof Document || node.getNodeType() == Node.DOCUMENT_NODE) {
            Node docNode = ((Document) node).getDocumentElement();
            jsonObjectBuilder.add(docNode.getNodeName(), processElement(docNode));
        } else if (node.getNodeType() == Node.ELEMENT_NODE) {
            jsonObjectBuilder.add(node.getNodeName(), processElement(node));
        } else {
            jsonObjectBuilder.add(node.getNodeName(), JsonValue.NULL);
        }

        return jsonObjectBuilder.build();
    }

    private JsonValue processElement(Node node) {
        JsonObjectBuilder jsonObjectBuilder = this.jsonBuilderFactory.createObjectBuilder();

        node.normalize();

        // PROCESS ATTRIBUTES
        if (node.getAttributes() != null && node.getAttributes().getLength() > 0) {
            for (int itemIndex = 0; itemIndex < node.getAttributes().getLength(); itemIndex++) {
                Node attribute = node.getAttributes().item(itemIndex);
                String attributeName = attribute.getNodeName();
                String attributeValue = attribute.getNodeValue() != null ? attribute.getNodeValue() : "";
                jsonObjectBuilder.add("@" + attributeName, attributeValue);
            }
        }
        // PROCESS CHILDREN NODES
        if (node.getChildNodes() != null && node.getChildNodes().getLength() > 0) {
            int textChild = 0;
            int cdataChild = 0;
            boolean hasElementChild = false;
            Map<String, Integer> nodeNameCount = new HashMap<String, Integer>();
            // vidi što nam je content node-a i popiši što si našao ...
            for (int childIndex = 0; childIndex < node.getChildNodes().getLength(); childIndex++) {
                if (node.getChildNodes().item(childIndex).getNodeType() == Node.ELEMENT_NODE) {
                    hasElementChild = true;
                } else if (node.getChildNodes().item(childIndex).getNodeType() == Node.TEXT_NODE) {
                    if (isNonWhitespaceText(node.getChildNodes().item(childIndex).getNodeValue())) {
                        textChild++;
                    }
                } else if (node.getChildNodes().item(childIndex).getNodeType() == Node.CDATA_SECTION_NODE) {
                    cdataChild++;
                }
                // pobroji elemente - treba nam kasnije za array kompoziciju
                if (nodeNameCount.containsKey(node.getChildNodes().item(childIndex).getNodeName())) {
                    nodeNameCount.put(node.getChildNodes().item(childIndex).getNodeName(), nodeNameCount.get(node.getChildNodes().item(childIndex).getNodeName()) + 1);
                } else {
                    nodeNameCount.put(node.getChildNodes().item(childIndex).getNodeName(), 1);
                }
            }
            // pripremi array buildere za sve nodove koji se više puta ponavljaju
            Map<String, JsonArrayBuilder> jsonArrayBuilders = new HashMap<String, JsonArrayBuilder>();
            for (Map.Entry<String, Integer> nodeNameCountEntry : nodeNameCount.entrySet()) {
                if (nodeNameCountEntry.getValue() > 1) {
                    jsonArrayBuilders.put(nodeNameCountEntry.getKey(), this.jsonBuilderFactory.createArrayBuilder());
                }
            }

            // parsiraj node content
            if (hasElementChild) {
                if (textChild < 2 && cdataChild < 2) {
                    Node child = node.getFirstChild();
                    while (child != null) {
                        if (child.getNodeType() == Node.TEXT_NODE) {
                            if (isNonWhitespaceText(child.getNodeValue())) {
                                jsonObjectBuilder.add("#text", escape(child.getNodeValue()));
                                child = child.getNextSibling();
                            } else {
                                Node nextSibling = node.getNextSibling();
                                node.removeChild(child);
                                child = nextSibling;
                            }
                        } else if (child.getNodeType() == Node.CDATA_SECTION_NODE) {
                            // dodaj u json
                            jsonObjectBuilder.add("#cdata", escape(child.getNodeValue()));
                            child = child.getNextSibling();
                        } else if (child.getNodeType() == Node.ELEMENT_NODE) {
                            // provjeri koliko se puta ponavlja element, ako je više onda gradi listu inače samo procesiraj element
                            if (nodeNameCount.get(child.getNodeName()) > 1) {
                                // dodaj zapis u array builder (nakon završetka while petlje ćemo dodati u json)
                                jsonArrayBuilders.get(child.getNodeName()).add(processElement(child));
                            } else {
                                jsonObjectBuilder.add(child.getNodeName(), processElement(child));
                            }
                            child = child.getNextSibling();
                        } else {
                            // nije ni element, ni text ni cdata - samo preskoči na sljedećeg
                            child = child.getNextSibling();
                        }
                    }
                    // dodaj array elemente u json
                    for (Map.Entry<String, JsonArrayBuilder> jsonArrayBuilderEntry : jsonArrayBuilders.entrySet()) {
                        jsonObjectBuilder.add(jsonArrayBuilderEntry.getKey(), jsonArrayBuilderEntry.getValue());
                    }
                } else { // mixed content
                    if (node.getAttributes() == null || node.getAttributes().getLength() == 0) {
                        return Json.createValue(escape(innerXml(node)));
                    } else {
                        jsonObjectBuilder.add("#text", escape(innerXml(node)));
                    }
                }
            } else if (textChild == 1) { // pure text
                if (node.getAttributes() == null || node.getAttributes().getLength() == 0) {
                    return Json.createValue(escape(innerXml(node)));
                } else {
                    jsonObjectBuilder.add("#text", escape(innerXml(node)));
                }
            } else if (cdataChild > 0) {
                if (cdataChild == 1) {
                    return Json.createValue(escape(innerXml(node)));
                } else {
                    JsonArrayBuilder jsonArrayBuilder = this.jsonBuilderFactory.createArrayBuilder();
                    Node child = node.getFirstChild();
                    while (child != null) {
                        jsonArrayBuilder.add(escape(innerXml(child)));
                        child = child.getNextSibling();
                    }
                    jsonObjectBuilder.add("#cdata", jsonArrayBuilder);
                }
            }
        }
        if ((node.getAttributes() == null || node.getAttributes().getLength() == 0) &&
                (node.getChildNodes() == null || node.getChildNodes().getLength() == 0)) {
            return JsonValue.NULL;
        }

        return jsonObjectBuilder.build();
    }

    private String escape(String txt) {
        return txt.replace("[\\]", "\\\\")
                .replace("[\"]", "\\")
                .replace("[\n]", "\\n")
                .replace("[\r]", "\\r");
    }

    // @see: https://stackoverflow.com/a/5948326
    private String innerXml(Node node) {
        DOMImplementationLS lsImpl = (DOMImplementationLS)node.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
        LSSerializer lsSerializer = lsImpl.createLSSerializer();
        NodeList childNodes = node.getChildNodes();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < childNodes.getLength(); i++) {
            sb.append(lsSerializer.writeToString(childNodes.item(i)));
        }
        return sb.toString();
    }

    private boolean isNonWhitespaceText(String txt) {
        return (txt != null && txt.trim().length() > 0);
    }

}
